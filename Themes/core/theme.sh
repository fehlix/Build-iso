#!/bin/bash

THEME_DIR=$(dirname $(readlink -f $0))
source $THEME_DIR/../theme-functions.sh
start_theme "$@"

copy_dir wallpaper/             	/usr/share/wallpaper/grub --create 

copy_file 10_linux 			/etc/grub.d/
copy_file 20_memtest86+ 	/etc/grub.d/
copy_file fbgrab 			/usr/bin/
copy_file live-to-installed 	/usr/sbin
copy_file process-reaper 	/usr/local/bin/
copy_file rc.local 			/etc/
copy_file rc.local.install      	/usr/share/antiX/ --create
copy_file .bashrc 			/etc/skel/
copy_file bootchartd.conf 	/etc/
copy_file grub 				/etc/default/
copy_file interfaces 		/etc/network/
copy_file issue			/usr/share/antiX/
copy_file resolvconf 		/etc/default/
copy_file sysctl.conf           	/etc/

exit